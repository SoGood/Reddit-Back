package com.sam.reddit;

import java.util.List;

import com.google.api.server.spi.config.Api;
import com.google.api.server.spi.config.ApiIssuer;
import com.google.api.server.spi.config.ApiMethod;
import com.google.api.server.spi.config.ApiNamespace;
import com.google.api.server.spi.config.Named;
import com.google.api.server.spi.config.Nullable;
import com.google.api.server.spi.response.BadRequestException;
import com.google.api.server.spi.response.NotFoundException;
import com.google.appengine.api.datastore.Query.SortDirection;
import com.sam.reddit.entities.MessageEntity;
import com.sam.reddit.repositories.MessageRepository;

@Api(
	    name = "messages",
	    version = "v1"
)
public class MessageEndpoint {

	MessageRepository mRepo = new MessageRepository();
	
	@ApiMethod(path="/{username}/messages/{id}", name="voteMessages", httpMethod="GET")
	public MessageEntity voterMessage(@Named("vote") boolean vote, @Named("username") String username, @Named("id") String id) throws NotFoundException, BadRequestException{
		
		MessageEntity mEntity = mRepo.getMassageById(id);
		
		boolean isSender = mEntity.getSender().equals(username);
		
		if(isSender) throw new BadRequestException("Tu ne peux pas voter ton propre message");
		
		mEntity = mRepo.voterMessage(username, id, vote);
		
		if(mEntity==null) throw new NotFoundException("Message non trouvé");
		
		
		return mEntity;
	}
	
	@ApiMethod(path="/{username}/messages", name="postMessages", httpMethod="POST")
	public MessageEntity postMessage(@Named("username") String idUser, MessageEntity meEntity){
		meEntity.setSender(idUser);
		return mRepo.postMessage(meEntity);
	}
	
	
	@ApiMethod(path="/{username}/messages", name="consultMyMessages", httpMethod="GET")
	public List<MessageEntity> consultMyMessages(@Named("username") String username, @Nullable @Named("limit") int limit){
		limit = limit > 0 ? limit : 100;
		return mRepo.getMessagesForSender(username, limit, SortDirection.DESCENDING);
	}
	
	@ApiMethod(path="/{username}/messages/voted", name="consultVotedMessagesByMe", httpMethod="GET")
	public List<MessageEntity> consultVotedMessagesByMe(@Named("username") String username, @Nullable @Named("limit") int limit){
		limit = limit > 0 ? limit : 100;
		return mRepo.getMessagesVotedBySender(username, limit);
	}
	
	
	@ApiMethod(path="/messages/karmaOrdered", name="listMessagesOrderedByKarma", httpMethod="GET")
	public List<MessageEntity>  getMessagesOrderedByKarma( @Nullable @Named("limit") int limit){
		limit = limit > 0 ? limit : 100;
		return mRepo.getMessagesOrderedByKarma(limit);
	}
	
	@ApiMethod(path="/messages", name="votantsByMessages", httpMethod="GET")
	public List<MessageEntity> getVotantsByMessages( @Named("nbVotants") int nbVotants, @Named("nbMessages") int nbMessages ){
		return mRepo.getVotantsByMessages(nbVotants, nbMessages);
	}
	
	
}
