package com.sam.reddit.entities;


import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.IdentityType;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;

import org.joda.time.DateTime;

import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.Text;

@PersistenceCapable(identityType=IdentityType.APPLICATION)

public class MessageEntity {
	
	@PrimaryKey
	@Persistent(valueStrategy=IdGeneratorStrategy.IDENTITY)
	private String id;
	
	@Persistent
	private Text body;
	
	@Persistent
	private String sender;
	
	@Persistent
	private int score;
	
	@Persistent
	private String creationDate;
	
	
	

	public MessageEntity() {
		super();
		this.score = 0;
		creationDate = new DateTime().toString();
	}

	public MessageEntity(String id, Text body, String sender, int score,  String creationDate) {
		super();
		this.id = id;
		this.body = body;
		this.sender = sender;
		this.score = score;
		this.creationDate = creationDate;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Text getBody() {
		return body;
	}

	public void setBody(Text body) {
		this.body = body;
	}

	public String getSender() {
		return sender;
	}

	public void setSender(String sender) {
		this.sender = sender;
	}

	public int getScore() {
		return score;
	}

	public void setScore(int score) {
		this.score = score;
	}
	
	public String getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(String creationDate) {
		this.creationDate = creationDate;
	}
	
	
	
}
