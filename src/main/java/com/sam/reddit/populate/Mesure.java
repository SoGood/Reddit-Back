package com.sam.reddit.populate;

import java.io.IOException;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.sam.reddit.repositories.MessageRepository;

public class Mesure extends HttpServlet {
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		
		MessageRepository mRepository = new MessageRepository();
		
		int [] nbDernierMessage = {10, 50, 100};
		
		int size = 100;
    	if(request.getParameter("size")!=null) {
    		
    		size = Integer.parseInt(request.getParameter("size")) ;
    	}
    	
    	for(int i : nbDernierMessage){
    		
    		response.getWriter().println("<h1>Les "+i+"  derniers messages</h1>");
    		response.getWriter().println("<table border='1'>");
    		
    		response.getWriter().println("<tr>");
				response.getWriter().println("<th>User</th>");
				response.getWriter().println("<th>Temps</th>");
			response.getWriter().println("</tr>");
    		for(int j=0; j<size; j++){
    			
    			long t1=System.currentTimeMillis();
    			
    			mRepository.getMessagesVotedBySender("u"+j, i);
    			
    			response.getWriter().println("<tr>");
					response.getWriter().println("<td>"+"u"+j+"</td>");
					response.getWriter().println("<td>"+(System.currentTimeMillis()-t1)+"ms</td>");
				response.getWriter().println("</tr>");
    			
    		}
    		response.getWriter().println("</table>");
    		
    		response.getWriter().println("<h1>Fini pour les "+i+"  derniers messages</h1>");
    	}
    	
	}
}
