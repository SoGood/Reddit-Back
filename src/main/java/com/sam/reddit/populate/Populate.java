package com.sam.reddit.populate;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.joda.time.DateTime;

import com.github.javafaker.Faker;
import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.Text;
import com.sam.reddit.entities.MessageEntity;
import com.sam.reddit.entities.MessageEntityIndex;

public class Populate extends HttpServlet {
	
	DatastoreService datastoreService = DatastoreServiceFactory.getDatastoreService();
	
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    		boolean createMessage = Boolean.parseBoolean(request.getParameter("createMessage")) ;
    		boolean vote = Boolean.parseBoolean(request.getParameter("vote")) ;
    		
    		int size = 100;
	    	if(request.getParameter("size")!=null) {
	    		
	    		size = Integer.parseInt(request.getParameter("size")) ;
	    	}
    		
    		if(createMessage){
    			generateMessages(request, response);
    		}
    		
    		if(vote){
    			try {
					List<Entity> messagesIndex = getVoters(size);
					datastoreService.put(messagesIndex);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
    		}
	}
	
	public void generateMessages(HttpServletRequest request, HttpServletResponse response) throws IOException {
		List<Entity> messages = this.getMessages();
		List<Entity> messagesIndex = this.getMessagesIndex();
		
		System.out.println(messagesIndex.size());
		
		try {
			datastoreService.put(messagesIndex);
			datastoreService.put(messages);
		} catch (Exception e) {
			// TODO: handle exception
		}
		
    }
	
	
	private List<Entity> getMessages(){
		List<Entity> entities = new ArrayList<Entity>();
		List<String> sender = this.getSender();
		
		Faker fake = new Faker();
		if(sender.size()>101) {
			for(int i=0; i<101; i++) {
				
				Entity m = new Entity(MessageEntity.class.getSimpleName(), "m"+i);
				m.setProperty("body", new Text(fake.lorem().paragraph()));
				m.setProperty("sender", sender.get(i));
				m.setProperty("score", 0);
				m.setProperty("creationDate", new DateTime().toString());
				entities.add(m);
			}
		}
		return entities;
	}
	
	private List<Entity> getMessagesIndex(){
		List<Entity> listMessageIndex = new ArrayList<Entity>();
		List<Entity> messages = getMessages();
		List<String> senders = getSender();
		int i = 0;
		for(Entity message : messages) {
			
			String vote = ":O";
			if(i%2 == 0 || i%9==0 ){
				vote = ":N";
			}
			
			List<String> votes = new ArrayList<String>();
			
			Entity mIndex = new Entity(MessageEntityIndex.class.getSimpleName(), "im"+i, message.getKey());
			mIndex.setProperty("voters", votes);
			
			listMessageIndex.add(mIndex);
			i++;
		}
		return listMessageIndex;
    }
	
	
	private List<Entity> getVoters(int size) throws Exception{
		
			List<Entity> listMIndex = getMessagesIndex();
			List<String> listSender = getSender();
			
			if(size>listSender.size()) throw new Exception("La configuration ne doit pas dépasser 5000");
			
			Random r=new Random();
			
			
			for(int j=0; j<listMIndex.size(); j++){
				List<String> votes = (List<String>) listMIndex.get(j).getProperty("voters");
				for(int i=0; i<size; i++) {
					votes.add(listSender.get(i)+":O");
				}
				listMIndex.get(j).setProperty("votes", votes);
			}
				
		return listMIndex;
	}
	

	
	private List<String> getSender(){
		
		List<String> listSender = new ArrayList<String>();
		
		//Faker fake = new Faker();
		
		for(int i=0; i<5000; i++){
			listSender.add("u"+i);
		}
		
		
		
		return listSender;
	}
}
