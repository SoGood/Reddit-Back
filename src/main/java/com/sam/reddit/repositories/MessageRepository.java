package com.sam.reddit.repositories;

import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.jdo.PersistenceManager;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.EntityNotFoundException;
import com.google.appengine.api.datastore.FetchOptions;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.PreparedQuery;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Query.Filter;
import com.google.appengine.api.datastore.Query.FilterOperator;
import com.google.appengine.api.datastore.Query.FilterPredicate;
import com.google.appengine.api.datastore.Query.SortDirection;
import com.google.appengine.api.datastore.Text;
import com.sam.reddit.entities.MessageEntity;
import com.sam.reddit.entities.MessageEntityIndex;

public class MessageRepository {
	
	private DatastoreService daService = DatastoreServiceFactory.getDatastoreService();
	
	public MessageEntity postMessage(MessageEntity meEntity){
		
		String idMessage = "m"+meEntity.hashCode();
		String idMessageIndex =  "im"+meEntity.hashCode();
		
		meEntity.setId(idMessage);
		
		
		MessageEntityIndex meIndex = new MessageEntityIndex(idMessageIndex, new ArrayList<String>());
		
		Entity m = new Entity(MessageEntity.class.getSimpleName(), idMessage);
		m.setProperty("body", meEntity.getBody());
		m.setProperty("sender", meEntity.getSender());
		m.setProperty("score", meEntity.getScore());
		m.setProperty("creationDate", meEntity.getCreationDate());
		
		
		Entity mIndex = new Entity(MessageEntityIndex.class.getSimpleName(), idMessageIndex, m.getKey());
		
		
		daService.put(mIndex);
		daService.put(m);
		
		return meEntity;
	}
	
	
	public MessageEntity voterMessage(String username, String messageId, boolean vote){
		
		String svote = vote ? "O" : "N";
		svote = username+":"+svote;
		
		Key mKey = KeyFactory.createKey(
				MessageEntity.class.getSimpleName(), 
				messageId
			);
		
		Key mIndKey = KeyFactory.createKey(
				mKey,
				MessageEntityIndex.class.getSimpleName(), 
				"i"+messageId
		);
		
		MessageEntity mEntity = null;
		boolean alreadyVoteYes = false;
		boolean alreadyVoteNo = false;
		
		try {
			Entity mIE = daService.get(mIndKey);
			MessageEntityIndex mIndex = new MessageEntityIndex(mIE.getKey().getName(), (List<String>) mIE.getProperty("voters"));
			Entity mE = daService.get(mKey);
			mEntity = new MessageEntity(mE.getKey().getName(),(Text) mE.getProperty("body"), (String) mE.getProperty("sender"), Integer.parseInt(mE.getProperty("score").toString()), (String) mE.getProperty("creationDate"));
			if(mIndex.getVoters() != null && mIndex.getVoters().contains(username+":O")){
				alreadyVoteYes = true;
				mIndex.getVoters().remove(username+":O");
			}else if(mIndex.getVoters() != null && mIndex.getVoters().contains(username+":N")){
				alreadyVoteNo = true;
				mIndex.getVoters().remove(username+":N");
			}
			
			int score = mEntity.getScore();
			int ivote = vote ? 1 : -1;
			
			if(alreadyVoteNo || alreadyVoteYes) {
				
				if(alreadyVoteNo && vote || alreadyVoteYes && !vote) {
					mEntity.setScore(score+ivote);
				}
				
			}else {
				if(score>0){
					mEntity.setScore(score+ivote);
				}else if(score==0 && vote){
					mEntity.setScore(score+ivote);
				}
			}
			
			System.out.println(mEntity.getScore()+" : "+ivote);
			if(mIndex.getVoters() == null) mIndex.setVoters(new ArrayList<String>());
			mIndex.getVoters().add(svote);
			mIE.setProperty("voters", mIndex.getVoters());
			daService.put(mIE);
			mE.setProperty("score", mEntity.getScore());
			daService.put(mE);
		} catch (EntityNotFoundException e) {

			System.out.println(e.getMessage());
		}

		return mEntity;
		
	}
	
	
	public List<MessageEntity> getMessagesForSender(String sender, int limit, SortDirection direction){
		
		Filter pf = new FilterPredicate("sender", FilterOperator.EQUAL, sender);
		Query q = new Query(MessageEntity.class.getSimpleName()).setFilter(pf).addSort("creationDate", direction);
		
		return this.getMessagesByQuery(q, limit);
	}
	
	public List<MessageEntity> getMessagesVotedBySender(String sender, int limit){
		
		List<String> etiqueta = new ArrayList<String>();
		etiqueta.add(sender+":O");
		etiqueta.add(sender+":N");

		
		Filter pf = new FilterPredicate("voters", FilterOperator.IN, etiqueta);
		//Filter pf1 = new FilterPredicate("voters", FilterOperator.EQUAL, sender+":N");
		Query q = new Query(MessageEntityIndex.class.getSimpleName()).setFilter(pf);
		
		q.setKeysOnly();

		
		PreparedQuery pq = daService.prepare(q);
		
		List<Entity> results = pq.asList(FetchOptions.Builder.withLimit(limit));
		
		List<Key> pk = new ArrayList<Key>();
		for (Entity r : results) {
			pk.add(r.getParent());
		}
		
		Map<Key, Entity> hMessages = new HashMap<Key, Entity>();
		hMessages = daService.get(pk);
		
		List<MessageEntity> entitiesMessage = new ArrayList<MessageEntity>();
		
		for (Entity entity : hMessages.values()) {
			entitiesMessage.add(this.buildMessageEntity(entity));
		}
		
		return entitiesMessage;
	}
	
	
	public List<MessageEntity> getMessagesOrderedByKarma(int limit){
		
		
		Query q = new Query(MessageEntity.class.getSimpleName()).addSort("score", SortDirection.DESCENDING);
		
		return this.getMessagesByQuery(q, limit);
	}
	
	
	private List<MessageEntity> getMessagesByQuery(Query q, int limit){
		
		PreparedQuery pq = daService.prepare(q);
		
		List<Entity> results = pq.asList(FetchOptions.Builder.withLimit(limit));
		
		List<MessageEntity> entitiesMessage = new ArrayList<MessageEntity>();
		
		for (Entity entity : results) {
			entitiesMessage.add(this.buildMessageEntity(entity));
		}
		
		return entitiesMessage;
	}
	
	private MessageEntity buildMessageEntity(Entity entity){
		return new MessageEntity(entity.getKey().getName(),(Text) entity.getProperty("body"), (String) entity.getProperty("sender"), Integer.parseInt(entity.getProperty("score").toString()),(String) entity.getProperty("creationDate"));
	}
	
	public MessageEntity getMassageById(String id){
		Key mKey = KeyFactory.createKey(
				MessageEntity.class.getSimpleName(), 
				id
			);
		MessageEntity meEntity = null;
		try{
			Entity message = daService.get(mKey);
			meEntity = this.buildMessageEntity(message);
		}catch (Exception e) {
			// TODO: handle exception
		}
		return meEntity;
	}
	
	public List<MessageEntity> getVotantsByMessages(int nbVotants, int nbMessages) {
		List<MessageEntity> myMessages = new ArrayList<MessageEntity>();
		DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();

		String user = "u1";
		for (int j = 0; j < 30; j++) {

			for (int i = 0; i < nbMessages; i++) {
				java.util.Random r = new java.util.Random();
				Entity e = new Entity("MessageEntity", "m" + i);
				e.setProperty("body", new Text("nope"));
				e.setProperty("sender", "u" + r.nextInt(nbVotants + 1));
				e.setProperty("score", 0);
				datastore.put(e);

				Entity index = new Entity("MessageEntityIndex", "im" + i, e.getKey());
				ArrayList<String> voters = new ArrayList<String>();
				for (int k = 0; k < nbVotants; k++) {
					voters.add("u" + r.nextInt(nbVotants + 1));
					this.voterMessage("u" + r.nextInt(nbVotants + 1), e.getKey().getName(), true);
				}
				index.setProperty("voters", voters);
				datastore.put(index);

				// insert entities to the list (il y aura des doublons 
				// au niveau des id des messages comme il y a 30 exécutions
				myMessages.addAll(this.getMessagesVotedBySender(user, nbMessages));
			}
			
		}
		return myMessages;
	}

}
